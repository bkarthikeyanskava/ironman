import * as React from 'react'
import { mycomponent, Text, Button } from 'my-play'

const LoaderPanel = () => {
 
  return (
    <React.Fragment>
      <Text />
      <Button text={mycomponent.name} />
    </React.Fragment>
  )
}

export { LoaderPanel }