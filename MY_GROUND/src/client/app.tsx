import * as React from 'react'
import { render } from 'react-dom'
import {LoaderPanel} from '../index'

const App = () => {
  return(
    <React.Fragment>
     <LoaderPanel />
    </React.Fragment>
  )
}

render(<App />, document.getElementById('app'))