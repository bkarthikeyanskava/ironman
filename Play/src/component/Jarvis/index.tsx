import * as React from 'react'
import { JarvisProps } from './typings'
import {
  defaultRenderImage,
  defaultRenderText,
  defaultOnImageItemClick,
  defaultOnTextItemClick,
} from './renderProps'

class Jarvis extends React.PureComponent<JarvisProps> {
  static defaultProps = {
    className: 'cls-base',
    text: 'base-text',
    pick: 'https://media.giphy.com/media/l3975CZuyQgoNVuOA/giphy.gif',
    renderImage: defaultRenderImage,
    renderText: defaultRenderText,
    onTextItemClick: defaultOnTextItemClick,
    onImageItemClick: defaultOnImageItemClick,
  }

  render() {
    const { renderImage, renderText, ...remainingProps } = this.props as any
    console.log('rendering Jarvis', this.props)
    return (
      <React.Fragment>
        {renderImage(remainingProps)}
        {renderText(remainingProps)}
      </React.Fragment>
    )
  }
}

export { Jarvis, JarvisProps };
export default Jarvis;
