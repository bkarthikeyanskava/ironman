import * as React from 'react'
import styled from "../../uxui/styled-modules";
import { JarvisProps } from './typings'

function createRenderImage(props: JarvisProps) {
  const { pick, onImageItemClick } = props
  return <img onClick={onImageItemClick} width='100px' src={pick} />
}

const TextWrapper = styled.h1`
color:tomato;
`
function createRenderText(props: JarvisProps) {
  const { text, className, onTextItemClick, ...balanceProps } = props
  return (
    <TextWrapper className={className} onClick={onTextItemClick} {...balanceProps}>
      {text}
    </TextWrapper>
  )
}

function createOnImageItemClick(event?: any) {
  console.log('I AM ATOM', event)
}

function createOnTextItemClick(event?: any) {
  console.log('I AM IRONMAN', event)
}

export {
  createRenderImage as defaultRenderImage,
  createRenderText as defaultRenderText,
  createOnImageItemClick as defaultOnImageItemClick,
  createOnTextItemClick as defaultOnTextItemClick,
  TextWrapper
}
