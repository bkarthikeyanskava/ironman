import * as React from 'react'
import { observer } from 'mobx-react'
import {FlipCardProps} from './typings'
import {FlipCardState,toFlipCardState} from './state'
import {StyledCardContainer,
        StyledCard,
        StyledFrontWrapper,
        StyledBackWrapper,
        defaultRenderFrontCardWrapper,
        defaultRenderBackCardWrapper} from './renderProps'
class FlipCard extends React.PureComponent <FlipCardProps>{
    static defaultProps ={
        className: '',
        index: '',
        renderFrontCardWrapper:defaultRenderFrontCardWrapper,
        renderBackCardWrapper:defaultRenderBackCardWrapper
    }
    observableState: FlipCardState = toFlipCardState(this.props)
    state:any = {
        height:0
    }
  
    render() {
        const {
            FlipCardState = this.observableState,
            className,
            renderFrontCardWrapper,
            renderBackCardWrapper
        } = this.props

        const StyledCardAttribute = {
            className:className,
            ...FlipCardState
        }

        return(
            <StyledCardContainer>
                <StyledCard {...StyledCardAttribute} >
                    <StyledFrontWrapper> 
                        {renderFrontCardWrapper(this)}
                    </StyledFrontWrapper>
                    <StyledBackWrapper>
                        {renderBackCardWrapper(this)}
                    </StyledBackWrapper>
                </StyledCard>
            </StyledCardContainer>
        )
    }
}
observer(FlipCard)

export {FlipCard}
export default FlipCard