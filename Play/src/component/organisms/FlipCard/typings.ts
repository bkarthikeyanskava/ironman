
export interface FlipCardProps {
    className?: string;
    renderFrontCard?: any;
    renderBackCard?: any;
    renderBox?: any;
    renderFrontCardWrapper?: any;
    renderBackCardWrapper?: any;
    FlipCardState?: any;
    state?: any;
    item?: any;
    index?: any;
  }


