import * as React from 'react'
import {CardAnatomyProps} from './typings'

import {defaultRenderImage, defaultRenderText} from './renderProps'

class CardAnatomy extends React.PureComponent <CardAnatomyProps>{
    static defaultProps ={
        ImageUrl: 'http://via.placeholder.com/350x150?text=ImageUrl',
        Title :'Title',
        SubTitle: 'SubTitle',
        description: 'description',
        className: 'cls-cardanatomy',
        renderImage : defaultRenderImage,
        renderText : defaultRenderText,
        
    }
    render(){
          const {renderImage, renderText, className, ...balanceProps} = this.props as any
          const attributes = {
              className: className
          }
          return(
            <div {...attributes}>
                {renderImage(balanceProps)}
                {renderText(balanceProps)}
            </div>
          )
    }

}

export {CardAnatomy}
export default CardAnatomy