export interface CardAnatomyProps {
    ImageUrl?: string;
    Title?:string;
    SubTitle?: string;
    Description?: string;
    className?: string;
  }