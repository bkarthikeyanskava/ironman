import * as React from 'react'
import TextElement from '../../atom/text'
import ImageElement from '../../atom/image'
import DescritionElement from '../../atom/descritpion'
import styled from '../../../uxui/styled-modules'

const StyledTitle = styled(TextElement)`
    color:green;
`
const Styleddescritpion =styled(DescritionElement) `
    color:pink;
`

const defaultRenderText = (props: any) => {
    const { Title, ...balanceProps } = props
    const attributes = {
        text: Title,
        ...balanceProps
    }
    return (
        <StyledTitle {...attributes} />
    );
}

const handleClick = (events: any) => {
    console.log('handleClick -------> ', events)
}

const defaultRenderImage = (props: any) => {
    const { ImageUrl } = props
    const imgAttributes = {
        imgurl: ImageUrl,
        onClick: handleClick
    }
    return(
        <ImageElement {...imgAttributes} />
    );
}

const defaultRenderDescritpion = (props: any) => {
    return(
        <Styleddescritpion {...props}/>
    );
}

export {defaultRenderText, defaultRenderImage, defaultRenderDescritpion}
