import * as React from 'react';
import { TextProps } from './typings'
import styled from '../../../uxui/styled-modules'

const TextWrapper = styled.div.attrs({
    className:''
})`
color:red;
`
class Text extends React.PureComponent<TextProps> {
    static defaultProps = {
        className: "cls-text",
        text: "sample",
    }
    render() {
        const { text, ...balanceProps } = this.props
        return (
            <React.Fragment>
                <TextWrapper {...balanceProps}>
                    {text}
                </TextWrapper>
            </React.Fragment>
        )
    }
}

export { Text, TextProps }
export default Text