import * as React from "react"
import { observer } from "mobx-react"
import CountState from "./state"
import {CounterProps} from "./typings"

/**
 * same as extendObservable({count: 0})
 * or decorate({increment: action.bound})
 */

const counterState = new CountState();
// @observer
class Counter extends React.PureComponent <CounterProps> {
  componentDidMount() {
    const onInterval = counterState.increment;
    setInterval(onInterval, 1000);
  }

  render() {
    return <h1>{counterState.count}</h1>;
  }
}

observer(Counter)

export default Counter
export{Counter}