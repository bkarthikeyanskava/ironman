export interface DescriptionProps {
    description?: string;
    className?: string;
  }