import * as React from 'react';
import { DescriptionProps } from './typings'
import DescriptionElement from '../text'
class Description extends React.PureComponent<DescriptionProps> {
    static defaultProps = {
        className: "cls-Description",
        description: "sample description",
    }
    render() {
        const { description, ...balanceProps } = this.props
        const attributes = {
            text:description,
            ...balanceProps
        }
        return (
            <React.Fragment>
                <DescriptionElement {...attributes} />
            </React.Fragment>
        )
    }
}

export { Description }
export default Description