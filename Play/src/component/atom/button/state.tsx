import { observable, action, decorate } from "mobx";


class ButtonState {
    isActive:boolean = false;

    setIsExpanded(isActive: boolean) {
      this.isActive = isActive
      return this
    }

   toggleIsActive() {
    // this.isActive = !this.isActive;
    this.setIsExpanded(!this.isActive)
   }
 }

 decorate(ButtonState, {
   isActive: observable,
   toggleIsActive: action
 })

 export default ButtonState
 export {ButtonState}