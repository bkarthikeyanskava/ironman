import * as React from "react"
import { observer } from "mobx-react"
import {ButtonState} from "./state"
import {ButtonProps} from "./typings"
import { renderClickProps} from "./renderProps"

// @observer
class Button extends React.PureComponent <ButtonProps> {

    static defaultProps = {
      text:'IRONMAN',
      state: new ButtonState(),
      renderClick: renderClickProps
    };

    handleClick = () => {
        const { renderClick, state} = this.props as any
        renderClick(state);
    }
    
    render() {
        const {text, state} = this.props as any
        const loadText = state.isActive ? "JARVIS" : text;
        return <button onClick={this.handleClick} > {loadText} </button>
    }
}
 observer(Button)

export default Button;
export{Button}