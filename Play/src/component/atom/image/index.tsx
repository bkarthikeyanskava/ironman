import * as React from 'react';
import {ImageProps} from './typings'

class Image extends React.PureComponent <ImageProps>{
    static defaultProps = {
        imgurl:'http://via.placeholder.com/350x150',
        className: 'cls-image',
    }

    render(){

        const {imgurl, ...balanceProps} = this.props
        return(
            <React.Fragment>
            <img src={imgurl} {...balanceProps} />
            </React.Fragment>
        )
    }



}


export {Image}

export default Image