import * as React from 'react'
import Card from './../../organisms/cardanatomy'
// import {Vision} from './jarvis'
// import {ProductProps} from './typings'

// @todo @fixme
class Product extends React.PureComponent<any> {
  static defaultProps = {
    ImageUrl: 'https://media.giphy.com/media/3o85xLjysBUs1cpdoA/giphy.gif',
    className: 'cls-card',
    Title:'Cartoon',
    SubTitle: 'SubTitle',
    description: 'Cartoon descrtiption',
  }
  render(){
    const {className, ...balanceProps} = this.props
    return(
        <Card className={className} {...balanceProps}/>
    )
  }
}

export {Product}
export default Product