import * as React from 'react'
import { render } from 'react-dom'
import {ContainerWrapper} from '../big-file'

const App = () => {
  return(
    <React.Fragment>
     <ContainerWrapper />
    </React.Fragment>
  )
}

render(<App />, document.getElementById('app'))

export {ContainerWrapper as Mykit}