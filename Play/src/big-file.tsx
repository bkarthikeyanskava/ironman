import * as React from 'react'
import {Product as GridCard} from './component/pages/product'
import {Counter} from './component/atom/counter'
import {Button, ButtonState, ButtonProps} from './component/atom/button'
import {FlipCard} from './component/organisms/FlipCard'
import { observer } from "mobx-react"
import styled from './uxui/styled-modules'


const StyledMainWrapperBackgroundColor= (props:any) => (props.isActive ? 'red' : 'green');
const StyledMainWrapper = styled.div.attrs({
    className:''
})`
background-color: ${StyledMainWrapperBackgroundColor};
width:100px;
height:20px;
border-radius:5px;
transition: all 400ms ease;
`

const {PureComponent, Fragment} = React
const ButtonStateUpdate = new ButtonState();
class ButtonItem extends PureComponent <ButtonProps> {
      static defaultProps = {
        text:'IRONMAN',
        state: ButtonStateUpdate,
      };
      handleClick = () => {
           this.props.state.toggleIsActive();
      }
      render() {
          const {text, state} = this.props as any
          const loadText = state.isActive ? "JARVIS" : text;
          return (
            <Fragment>
              <StyledMainWrapper {...state} />
              <button onClick={this.handleClick} > {loadText} </button>
            </Fragment>
          )
      }
  }
  observer(ButtonItem)


const ContainerWrapper = () => {

  const HandleClickEvent: any = (event: any) => {
    event.toggleIsActive();
    console.log(event.isActive);
    console.log('--->', ButtonStateUpdate)
  };
  var list = ['IRONMAN', 'SPIDERMAN', 'BATMAN', 'ANTMAN'] as any
  const FlipCardList = () => {
      return list.map((item:string, index:number) => <FlipCard  className={'cls-FlipCard-' + index} item={item} index={index} />)
  }
  return (
    <React.Fragment>
      <GridCard />
      <Counter/>
      <Button renderClick={HandleClickEvent} text={"tony"} />
      <ButtonItem></ButtonItem>
      {FlipCardList()}
    </React.Fragment>
    
  )
}

export {ContainerWrapper}