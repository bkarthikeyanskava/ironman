/// <reference types="react" />
import * as styledComponents from "styled-components";
declare const styled: styledComponents.ThemedBaseStyledInterface<IThemeInterface>, css: styledComponents.ThemedCssFunction<IThemeInterface>, injectGlobal: (strings: TemplateStringsArray, ...interpolations: styledComponents.SimpleInterpolation[]) => void, keyframes: (strings: TemplateStringsArray, ...interpolations: styledComponents.SimpleInterpolation[]) => string, ThemeProvider: import("react").ComponentClass<styledComponents.ThemeProviderProps<IThemeInterface>, any>;
export interface IThemeInterface {
    primaryColor: string;
}
export declare const theme: {
    primaryColor: string;
};
export default styled;
export { css, injectGlobal, keyframes, ThemeProvider };
