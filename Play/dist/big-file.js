import * as React from 'react';
import { Product as GridCard } from './component/pages/product';
import { Counter } from './component/atom/counter';
import { Button, ButtonState } from './component/atom/button';
import { FlipCard } from './component/organisms/FlipCard';
import { observer } from "mobx-react";
import styled from './uxui/styled-modules';
const StyledMainWrapperBackgroundColor = (props) => (props.isActive ? 'red' : 'green');
const StyledMainWrapper = styled.div.attrs({
    className: ''
}) `
background-color: ${StyledMainWrapperBackgroundColor};
width:100px;
height:20px;
border-radius:5px;
transition: all 400ms ease;
`;
const { PureComponent, Fragment } = React;
const ButtonStateUpdate = new ButtonState();
class ButtonItem extends PureComponent {
    constructor() {
        super(...arguments);
        this.handleClick = () => {
            this.props.state.toggleIsActive();
        };
    }
    render() {
        const { text, state } = this.props;
        const loadText = state.isActive ? "JARVIS" : text;
        return (React.createElement(Fragment, null,
            React.createElement(StyledMainWrapper, Object.assign({}, state)),
            React.createElement("button", { onClick: this.handleClick },
                " ",
                loadText,
                " ")));
    }
}
ButtonItem.defaultProps = {
    text: 'IRONMAN',
    state: ButtonStateUpdate,
};
observer(ButtonItem);
const ContainerWrapper = () => {
    const HandleClickEvent = (event) => {
        event.toggleIsActive();
        console.log(event.isActive);
        console.log('--->', ButtonStateUpdate);
    };
    var list = ['IRONMAN', 'SPIDERMAN', 'BATMAN', 'ANTMAN'];
    const FlipCardList = () => {
        return list.map((item, index) => React.createElement(FlipCard, { className: 'cls-FlipCard-' + index, item: item, index: index }));
    };
    return (React.createElement(React.Fragment, null,
        React.createElement(GridCard, null),
        React.createElement(Counter, null),
        React.createElement(Button, { renderClick: HandleClickEvent, text: "tony" }),
        React.createElement(ButtonItem, null),
        FlipCardList()));
};
export { ContainerWrapper };
//# sourceMappingURL=big-file.js.map