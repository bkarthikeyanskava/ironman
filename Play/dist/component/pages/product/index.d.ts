import * as React from 'react';
import { ProductProps } from './typings';
declare class Product extends React.PureComponent<ProductProps> {
    static defaultProps: {
        ImageUrl: string;
        className: string;
        Title: string;
        SubTitle: string;
        description: string;
    };
    render(): JSX.Element;
}
export { Product };
export default Product;
