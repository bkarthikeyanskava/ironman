var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
import Card from './../../organisms/cardanatomy';
// import {Vision} from './jarvis'
// import {ProductProps} from './typings'
// @todo @fixme
class Product extends React.PureComponent {
    render() {
        const _a = this.props, { className } = _a, balanceProps = __rest(_a, ["className"]);
        return (React.createElement(Card, Object.assign({ className: className }, balanceProps)));
    }
}
Product.defaultProps = {
    ImageUrl: 'https://media.giphy.com/media/3o85xLjysBUs1cpdoA/giphy.gif',
    className: 'cls-card',
    Title: 'Cartoon',
    SubTitle: 'SubTitle',
    description: 'Cartoon descrtiption',
};
export { Product };
export default Product;
//# sourceMappingURL=index.js.map