import { observable, action, decorate } from "mobx";
class ButtonState {
    constructor() {
        this.isActive = false;
    }
    setIsExpanded(isActive) {
        this.isActive = isActive;
        return this;
    }
    toggleIsActive() {
        // this.isActive = !this.isActive;
        this.setIsExpanded(!this.isActive);
    }
}
decorate(ButtonState, {
    isActive: observable,
    toggleIsActive: action
});
export default ButtonState;
export { ButtonState };
//# sourceMappingURL=state.js.map