declare class ButtonState {
    isActive: boolean;
    setIsExpanded(isActive: boolean): this;
    toggleIsActive(): void;
}
export default ButtonState;
export { ButtonState };
