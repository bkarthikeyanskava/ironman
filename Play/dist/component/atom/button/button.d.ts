import * as React from "react";
import { ButtonState } from "./state";
import { ButtonProps } from "./typings";
import { renderClickProps } from "./renderProps";
declare class Button extends React.PureComponent<ButtonProps> {
    static defaultProps: {
        text: string;
        state: ButtonState;
        renderClick: typeof renderClickProps;
    };
    handleClick: () => void;
    render(): JSX.Element;
}
export default Button;
export { Button };
