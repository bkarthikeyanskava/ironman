interface HandleClickEventProp extends Function {
    (event: any): void;
}
export interface ButtonProps {
    text?: string;
    state?: any;
    renderClick?: HandleClickEventProp;
}
export {};
