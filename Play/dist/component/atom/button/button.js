import * as React from "react";
import { observer } from "mobx-react";
import { ButtonState } from "./state";
import { renderClickProps } from "./renderProps";
// @observer
class Button extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.handleClick = () => {
            const { renderClick, state } = this.props;
            renderClick(state);
        };
    }
    render() {
        const { text, state } = this.props;
        const loadText = state.isActive ? "JARVIS" : text;
        return React.createElement("button", { onClick: this.handleClick },
            " ",
            loadText,
            " ");
    }
}
Button.defaultProps = {
    text: 'IRONMAN',
    state: new ButtonState(),
    renderClick: renderClickProps
};
observer(Button);
export default Button;
export { Button };
//# sourceMappingURL=button.js.map