var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
import styled from '../../../uxui/styled-modules';
const TextWrapper = styled.div.attrs({
    className: ''
}) `
color:red;
`;
class Text extends React.PureComponent {
    render() {
        const _a = this.props, { text } = _a, balanceProps = __rest(_a, ["text"]);
        return (React.createElement(React.Fragment, null,
            React.createElement(TextWrapper, Object.assign({}, balanceProps), text)));
    }
}
Text.defaultProps = {
    className: "cls-text",
    text: "sample",
};
export { Text };
export default Text;
//# sourceMappingURL=index.js.map