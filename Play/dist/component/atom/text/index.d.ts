import * as React from 'react';
import { TextProps } from './typings';
declare class Text extends React.PureComponent<TextProps> {
    static defaultProps: {
        className: string;
        text: string;
    };
    render(): JSX.Element;
}
export { Text, TextProps };
export default Text;
