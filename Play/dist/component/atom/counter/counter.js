import * as React from "react";
import { observer } from "mobx-react";
import CountState from "./state";
/**
 * same as extendObservable({count: 0})
 * or decorate({increment: action.bound})
 */
const counterState = new CountState();
// @observer
class Counter extends React.PureComponent {
    componentDidMount() {
        const onInterval = counterState.increment;
        setInterval(onInterval, 1000);
    }
    render() {
        return React.createElement("h1", null, counterState.count);
    }
}
observer(Counter);
export default Counter;
export { Counter };
//# sourceMappingURL=counter.js.map