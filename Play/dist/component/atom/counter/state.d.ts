declare class counterState {
    count: number;
    increment(): void;
}
export default counterState;
export { counterState };
