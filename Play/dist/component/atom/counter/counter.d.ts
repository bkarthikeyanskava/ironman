import * as React from "react";
import { CounterProps } from "./typings";
declare class Counter extends React.PureComponent<CounterProps> {
    componentDidMount(): void;
    render(): JSX.Element;
}
export default Counter;
export { Counter };
