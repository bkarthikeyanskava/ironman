import { observable, action, decorate } from "mobx";
class counterState {
    constructor() {
        // @observable count = 0;
        // @action.bound
        this.count = 0;
    }
    increment() {
        this.count += 1;
    }
}
decorate(counterState, {
    count: observable,
    increment: action.bound
});
export default counterState;
export { counterState };
//# sourceMappingURL=state.js.map