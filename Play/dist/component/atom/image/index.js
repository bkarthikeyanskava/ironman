var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
class Image extends React.PureComponent {
    render() {
        const _a = this.props, { imgurl } = _a, balanceProps = __rest(_a, ["imgurl"]);
        return (React.createElement(React.Fragment, null,
            React.createElement("img", Object.assign({ src: imgurl }, balanceProps))));
    }
}
Image.defaultProps = {
    imgurl: 'http://via.placeholder.com/350x150',
    className: 'cls-image',
};
export { Image };
export default Image;
//# sourceMappingURL=index.js.map