import * as React from 'react';
import { ImageProps } from './typings';
declare class Image extends React.PureComponent<ImageProps> {
    static defaultProps: {
        imgurl: string;
        className: string;
    };
    render(): JSX.Element;
}
export { Image };
export default Image;
