var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
import DescriptionElement from '../text';
class Description extends React.PureComponent {
    render() {
        const _a = this.props, { description } = _a, balanceProps = __rest(_a, ["description"]);
        const attributes = Object.assign({ text: description }, balanceProps);
        return (React.createElement(React.Fragment, null,
            React.createElement(DescriptionElement, Object.assign({}, attributes))));
    }
}
Description.defaultProps = {
    className: "cls-Description",
    description: "sample description",
};
export { Description };
export default Description;
//# sourceMappingURL=index.js.map