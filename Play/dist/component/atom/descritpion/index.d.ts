import * as React from 'react';
import { DescriptionProps } from './typings';
declare class Description extends React.PureComponent<DescriptionProps> {
    static defaultProps: {
        className: string;
        description: string;
    };
    render(): JSX.Element;
}
export { Description };
export default Description;
