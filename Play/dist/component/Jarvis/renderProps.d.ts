import * as React from 'react';
import { JarvisProps } from './typings';
declare function createRenderImage(props: JarvisProps): JSX.Element;
declare const TextWrapper: import("styled-components").StyledComponentClass<React.DetailedHTMLProps<React.HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>, import("../../uxui/styled-modules").IThemeInterface, React.DetailedHTMLProps<React.HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>>;
declare function createRenderText(props: JarvisProps): JSX.Element;
declare function createOnImageItemClick(event?: any): void;
declare function createOnTextItemClick(event?: any): void;
export { createRenderImage as defaultRenderImage, createRenderText as defaultRenderText, createOnImageItemClick as defaultOnImageItemClick, createOnTextItemClick as defaultOnTextItemClick, TextWrapper };
