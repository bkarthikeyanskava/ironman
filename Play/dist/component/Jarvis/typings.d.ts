import { ReactNode } from "react";
interface JarvisHandleEventProp extends Function {
    (event: any): void;
}
export interface JarvisNodeProps extends Function {
    (props: listProps): ReactNode;
}
export interface listProps {
    label: string;
    value: string | number;
    image: string;
}
export interface JarvisProps {
    text?: string;
    className?: string;
    pick?: string;
    renderText?: JarvisNodeProps;
    renderImage?: JarvisNodeProps;
    onTextItemClick?: JarvisHandleEventProp;
    onImageItemClick?: JarvisHandleEventProp;
}
export {};
