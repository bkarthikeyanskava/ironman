var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
import styled from "../../uxui/styled-modules";
function createRenderImage(props) {
    const { pick, onImageItemClick } = props;
    return React.createElement("img", { onClick: onImageItemClick, width: '100px', src: pick });
}
const TextWrapper = styled.h1 `
color:tomato;
`;
function createRenderText(props) {
    const { text, className, onTextItemClick } = props, balanceProps = __rest(props, ["text", "className", "onTextItemClick"]);
    return (React.createElement(TextWrapper, Object.assign({ className: className, onClick: onTextItemClick }, balanceProps), text));
}
function createOnImageItemClick(event) {
    console.log('I AM ATOM', event);
}
function createOnTextItemClick(event) {
    console.log('I AM IRONMAN', event);
}
export { createRenderImage as defaultRenderImage, createRenderText as defaultRenderText, createOnImageItemClick as defaultOnImageItemClick, createOnTextItemClick as defaultOnTextItemClick, TextWrapper };
//# sourceMappingURL=renderProps.js.map