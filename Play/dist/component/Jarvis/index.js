var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
import { defaultRenderImage, defaultRenderText, defaultOnImageItemClick, defaultOnTextItemClick, } from './renderProps';
class Jarvis extends React.PureComponent {
    render() {
        const _a = this.props, { renderImage, renderText } = _a, remainingProps = __rest(_a, ["renderImage", "renderText"]);
        console.log('rendering Jarvis', this.props);
        return (React.createElement(React.Fragment, null,
            renderImage(remainingProps),
            renderText(remainingProps)));
    }
}
Jarvis.defaultProps = {
    className: 'cls-base',
    text: 'base-text',
    pick: 'https://media.giphy.com/media/l3975CZuyQgoNVuOA/giphy.gif',
    renderImage: defaultRenderImage,
    renderText: defaultRenderText,
    onTextItemClick: defaultOnTextItemClick,
    onImageItemClick: defaultOnImageItemClick,
};
export { Jarvis };
export default Jarvis;
//# sourceMappingURL=index.js.map