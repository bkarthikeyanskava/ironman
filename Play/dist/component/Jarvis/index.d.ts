import * as React from 'react';
import { JarvisProps } from './typings';
import { defaultRenderImage, defaultRenderText, defaultOnImageItemClick, defaultOnTextItemClick } from './renderProps';
declare class Jarvis extends React.PureComponent<JarvisProps> {
    static defaultProps: {
        className: string;
        text: string;
        pick: string;
        renderImage: typeof defaultRenderImage;
        renderText: typeof defaultRenderText;
        onTextItemClick: typeof defaultOnTextItemClick;
        onImageItemClick: typeof defaultOnImageItemClick;
    };
    render(): JSX.Element;
}
export { Jarvis, JarvisProps };
export default Jarvis;
