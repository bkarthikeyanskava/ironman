import * as React from 'react';
import { CardAnatomyProps } from './typings';
declare class CardAnatomy extends React.PureComponent<CardAnatomyProps> {
    static defaultProps: {
        ImageUrl: string;
        Title: string;
        SubTitle: string;
        description: string;
        className: string;
        renderImage: (props: any) => JSX.Element;
        renderText: (props: any) => JSX.Element;
    };
    render(): JSX.Element;
}
export { CardAnatomy };
export default CardAnatomy;
