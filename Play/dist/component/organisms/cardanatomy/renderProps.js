var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
import TextElement from '../../atom/text';
import ImageElement from '../../atom/image';
import DescritionElement from '../../atom/descritpion';
import styled from '../../../uxui/styled-modules';
const StyledTitle = styled(TextElement) `
    color:green;
`;
const Styleddescritpion = styled(DescritionElement) `
    color:pink;
`;
const defaultRenderText = (props) => {
    const { Title } = props, balanceProps = __rest(props, ["Title"]);
    const attributes = Object.assign({ text: Title }, balanceProps);
    return (React.createElement(StyledTitle, Object.assign({}, attributes)));
};
const handleClick = (events) => {
    console.log('handleClick -------> ', events);
};
const defaultRenderImage = (props) => {
    const { ImageUrl } = props;
    const imgAttributes = {
        imgurl: ImageUrl,
        onClick: handleClick
    };
    return (React.createElement(ImageElement, Object.assign({}, imgAttributes)));
};
const defaultRenderDescritpion = (props) => {
    return (React.createElement(Styleddescritpion, Object.assign({}, props)));
};
export { defaultRenderText, defaultRenderImage, defaultRenderDescritpion };
//# sourceMappingURL=renderProps.js.map