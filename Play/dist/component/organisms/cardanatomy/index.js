var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
import * as React from 'react';
import { defaultRenderImage, defaultRenderText } from './renderProps';
class CardAnatomy extends React.PureComponent {
    render() {
        const _a = this.props, { renderImage, renderText, className } = _a, balanceProps = __rest(_a, ["renderImage", "renderText", "className"]);
        const attributes = {
            className: className
        };
        return (React.createElement("div", Object.assign({}, attributes),
            renderImage(balanceProps),
            renderText(balanceProps)));
    }
}
CardAnatomy.defaultProps = {
    ImageUrl: 'http://via.placeholder.com/350x150?text=ImageUrl',
    Title: 'Title',
    SubTitle: 'SubTitle',
    description: 'description',
    className: 'cls-cardanatomy',
    renderImage: defaultRenderImage,
    renderText: defaultRenderText,
};
export { CardAnatomy };
export default CardAnatomy;
//# sourceMappingURL=index.js.map