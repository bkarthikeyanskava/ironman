import * as React from 'react';
import { observer } from 'mobx-react';
import { toFlipCardState } from './state';
import { StyledCardContainer, StyledCard, StyledFrontWrapper, StyledBackWrapper, defaultRenderFrontCardWrapper, defaultRenderBackCardWrapper } from './renderProps';
class FlipCard extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.observableState = toFlipCardState(this.props);
        this.state = {
            height: 0
        };
    }
    render() {
        const { FlipCardState = this.observableState, className, renderFrontCardWrapper, renderBackCardWrapper } = this.props;
        const StyledCardAttribute = Object.assign({ className: className }, FlipCardState);
        return (React.createElement(StyledCardContainer, null,
            React.createElement(StyledCard, Object.assign({}, StyledCardAttribute),
                React.createElement(StyledFrontWrapper, null, renderFrontCardWrapper(this)),
                React.createElement(StyledBackWrapper, null, renderBackCardWrapper(this)))));
    }
}
FlipCard.defaultProps = {
    className: '',
    index: '',
    renderFrontCardWrapper: defaultRenderFrontCardWrapper,
    renderBackCardWrapper: defaultRenderBackCardWrapper
};
observer(FlipCard);
export { FlipCard };
export default FlipCard;
//# sourceMappingURL=AnimationCard.js.map