import { FlipCardProps } from "./typings";
declare class FlipCardState {
    isActive: boolean;
    setIsFlip(isActive: boolean): this;
    toggleOpen(): void;
    toggleClose(): void;
    toggleIsActive(isFlag?: boolean): void;
}
declare const toFlipCardState: (props: FlipCardProps) => any;
export default FlipCardState;
export { FlipCardState, toFlipCardState };
