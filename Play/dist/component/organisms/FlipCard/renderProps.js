import * as React from 'react';
import styled from '../../../uxui/styled-modules';
const StyledCardContainer = styled.div.attrs({
    className: ''
}) `
    cursor: pointer;
    height: 150px;
    perspective: 600;
    position: relative;
    width: 150px;
    margin: 10px;
`;
const StyledFlipAnimation = (props) => (props.isActive ? 'transform: rotateY(180deg)' : 'transform: rotateY(0deg)');
const StyledCard = styled.div.attrs({
    className: '',
}) `
    height: 100%;
    position: absolute;
    transform-style: preserve-3d;
    transition: all 400ms ease-in-out;
    width: 100%;
    transform: rotateY(180deg);
    ${StyledFlipAnimation}
`;
const StyledFrontWrapper = styled.div.attrs({
    className: ''
}) `
background-color: #CCC;
    backface-visibility: hidden;
    border-radius: 6px;
    height: 100%;
    position: absolute;
    overflow: hidden;
    width: 100%;
`;
const StyledBackWrapper = styled.div.attrs({
    className: ''
}) `
    background-color: #999;
    backface-visibility: hidden;
    border-radius: 6px;
    height: 100%;
    position: absolute;
    overflow: hidden;
    width: 100%;
    transform: rotateY(180deg);
`;
let pushed = [];
const defaultRenderFrontCardWrapper = (props) => {
    const handleOpenCardClick = () => {
        if (pushed.length > 0) {
            let currentElement = pushed[0];
            if (currentElement.props.index !== props.props.index) {
                if (currentElement.observableState.isActive === true) {
                    currentElement.observableState.toggleClose();
                }
                pushed = [];
            }
        }
        if (props.observableState.isActive === true) {
            /* @todo flag put in bgClick activation
            props.observableState.toggleClose() */
        }
        else {
            if (pushed.length == 0) {
                pushed.push(props);
            }
            props.observableState.toggleIsActive();
        }
    };
    const { item } = props.props;
    console.log(props);
    return (React.createElement(React.Fragment, null,
        item,
        React.createElement("input", Object.assign({ onClick: handleOpenCardClick }, props, { type: "button", value: "OPEN" }))));
};
const defaultRenderBackCardWrapper = (props) => {
    const handleOpenCardClick = () => {
        props.observableState.toggleClose();
    };
    return React.createElement("input", Object.assign({ onClick: handleOpenCardClick }, props, { type: "button", value: "close" }));
};
export { StyledCardContainer, StyledCard, StyledFrontWrapper, StyledBackWrapper, defaultRenderFrontCardWrapper, defaultRenderBackCardWrapper };
//# sourceMappingURL=renderProps.js.map