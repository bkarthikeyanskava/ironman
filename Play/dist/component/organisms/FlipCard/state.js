import { observable, action, decorate } from "mobx";
class FlipCardState {
    constructor() {
        this.isActive = false;
    }
    setIsFlip(isActive) {
        this.isActive = isActive;
        return this;
    }
    toggleOpen() {
        this.setIsFlip(true);
    }
    toggleClose() {
        this.setIsFlip(false);
    }
    toggleIsActive(isFlag) {
        const isTempFlag = isFlag || !this.isActive;
        this.setIsFlip(isTempFlag);
    }
}
decorate(FlipCardState, {
    isActive: observable,
    toggleIsActive: action,
    toggleOpen: action,
    toggleClose: action
});
const initFlipCardState = () => {
    // console.debug('[ExpandableCard] creating default state')
    return new FlipCardState();
};
const toFlipCardState = (props) => {
    if (props.state !== undefined) {
        // console.debug('[ExpandableCard] passed in state via props')
        return props.state;
    }
    else {
        return initFlipCardState();
    }
};
export default FlipCardState;
export { FlipCardState, toFlipCardState };
//# sourceMappingURL=state.js.map