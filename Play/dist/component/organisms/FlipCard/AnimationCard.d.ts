import * as React from 'react';
import { FlipCardProps } from './typings';
import { FlipCardState } from './state';
declare class FlipCard extends React.PureComponent<FlipCardProps> {
    static defaultProps: {
        className: string;
        index: string;
        renderFrontCardWrapper: (props: any) => JSX.Element;
        renderBackCardWrapper: (props: any) => JSX.Element;
    };
    observableState: FlipCardState;
    state: any;
    render(): JSX.Element;
}
export { FlipCard };
export default FlipCard;
