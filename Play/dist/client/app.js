import * as React from 'react';
import { render } from 'react-dom';
import { ContainerWrapper } from '../big-file';
const App = () => {
    return (React.createElement(React.Fragment, null,
        React.createElement(ContainerWrapper, null)));
};
render(React.createElement(App, null), document.getElementById('app'));
export { ContainerWrapper as Mykit };
//# sourceMappingURL=app.js.map